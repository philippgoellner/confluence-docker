#!/bin/sh

env CATALINA_OPTS="-Xms${RAM_MIN}m -Xmx${RAM_MAX}m $CATALINA_OPTS"

if [ "$CONF_REMOTE_DEBUG" = "true" ]; then
	echo "set remote debugging port"
	CATALINA_OPTS="-Xdebug -Xrunjdwp:transport=dt_socket,address=$DEBUG_PORT,server=y,suspend=n $CATALINA_OPTS"
fi

if [ "$PROXY_NAME" != "false" ]  && [ ! -f ${CONF_INSTALL}/PROXYNAME ]; then
	echo "manipulate proxy name in server.xml"
    xmlstarlet ed -i /Server/Service/Connector -t attr -n proxyName -v ${PROXY_NAME} ${CONF_INSTALL}/conf/server.xml > /tmp/generated-server.xml
    mv /tmp/generated-server.xml ${CONF_INSTALL}/conf/server.xml
	touch ${CONF_INSTALL}/PROXYNAME

fi

if [ "$HTTPS" = "true" ] && [ ! -f ${CONF_INSTALL}/HTTPS ]; then
	echo "enable https"
    xmlstarlet ed -i /Server/Service/Connector -t attr -n proxyPort -v 443 ${CONF_INSTALL}/conf/server.xml > /tmp/generated-server.xml
    mv /tmp/generated-server.xml ${CONF_INSTALL}/conf/server.xml
    xmlstarlet ed -i /Server/Service/Connector -t attr -n scheme -v https ${CONF_INSTALL}/conf/server.xml > /tmp/generated-server.xml
    mv /tmp/generated-server.xml ${CONF_INSTALL}/conf/server.xml
	touch ${CONF_INSTALL}/HTTPS
fi

if [ "$IMPORTCERT" = "true" ]; then 
	CATALINA_OPTS="-Djavax.net.ssl.trustStore=/opt/jdk/current/jre/lib/security/cacerts -Djavax.net.ssl.trustStorePassword=changeit $CATALINA_OPTS"      

	cd ${IMPORTPATH}
	if [ ! -f ${CONF_INSTALL}/skipcert.conf ]; then
		for i in *
		do 
			/usr/lib/jvm/java-8-oracle/jre/bin/keytool -keystore /usr/lib/jvm/java-8-oracle/jre/lib/security/cacerts -importcert -alias $i -file $i -storepass changeit -noprompt
		done
		touch ${CONF_INSTALL}/skipcert.conf
	fi
	cd ${CONF_INSTALL}
fi

if [ "$NEWRELIC" = "true" ]; then
	unzip newrelic-java-${NEWRELIC_VERSION}.zip -d ${CONF_INSTALL}

	cd ${CONF_INSTALL}/newrelic
	java -jar newrelic.jar install
	sed -ri "s/app_name:.*$/app_name: ${NEWRELIC_APP_NAME}/" newrelic.yml
	sed -ri "s/license_key:[ \t]*'[^']+'/license_key: '${NEWRELIC_LICENSE}'/" newrelic.yml
fi

if [ "$WAIT" = "true" ]; then

	is_ready() {
    		eval "$WAIT_COMMAND"
	}

	# wait until is ready
	i=0
	while ! is_ready; do
    	i=`expr $i + 1`
    	if [ $i -ge ${WAIT_LOOPS} ]; then
        	echo "$(date) - still not ready, giving up"
        	exit 1
    	fi
    	echo "WAITING: $(date) - waiting to be ready for ${WAIT_COMMAND}"
    	sleep ${WAIT_SLEEP}
	done
fi

${CONF_INSTALL}/bin/start-confluence.sh -fg
