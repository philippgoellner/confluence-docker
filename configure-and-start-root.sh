#!/bin/sh
mkdir -p ${CONF_HOME}
chown -R ${RUN_USER}: ${CONF_HOME}

# Set time zone if provided
if [ "${TIME_ZONE}" != "false" ]; then
    ln -fs /usr/share/zoneinfo/${TIME_ZONE} /etc/localtime
    echo "${TIME_ZONE}" > /etc/timezone
fi

# Fetch backup data over the network
if [ "${BACKUP_HOST}" != "false" ] && [ ! -f ${CONF_HOME}/DONTSYNC ]; then
    chmod 400 ${BACKUP_KEY_FILE}
    echo "Start restoration of backup data over the network from ${BACKUP_HOST}:"
    rsync -arczv -e "ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${BACKUP_KEY_FILE}" ${BACKUP_USER}@${BACKUP_HOST}:${BACKUP_PATH}/* ${CONF_HOME}
    if [ $? -eq 0 ]; then
        echo "Successfully restored backup data over the network from ${BACKUP_HOST}"
    else
        echo "Restoration of backup data over the network from ${BACKUP_HOST} failed"
        DO_NOT_START=true
    fi
fi

# Apply changes to the connected database
xmlstarlet sel -T -t -v "/confluence-configuration/properties/property[@name='hibernate.connection.url']" -n ${CONF_HOME}/confluence.cfg.xml | grep mysql: > /dev/null
if [ $? -eq 0 ]; then
    DB_TYPE=mysql
fi
# If Confluence uses MySQL
if [ ${DB_TYPE} = "mysql" ]; then
    if [ -z "${DB_DB}" ]; then
        DB_DB=$(xmlstarlet sel -T -t -v "/confluence-configuration/properties/property[@name='hibernate.connection.url']" -n ${CONF_HOME}/confluence.cfg.xml | cut -d"/" -f4 | cut -d"?" -f1)
    fi
    DB_COMMANDS=/root/commands.sql
    rm -f ${DB_COMMANDS}
    if [ ! -z "${DB_BASEURL}" ]; then
        cat << EOF >> ${DB_COMMANDS}
SET @DB_BASEURL_OLD := (SELECT SUBSTR(BANDANAVALUE, LOCATE('<baseUrl>', BANDANAVALUE)+9, LOCATE('</baseUrl>', BANDANAVALUE)-(LOCATE('<baseUrl>', BANDANAVALUE)+9)) FROM ${DB_DB}.BANDANA WHERE BANDANAKEY="atlassian.confluence.settings");
UPDATE ${DB_DB}.AO_9412A1_AOREGISTRATION SET DATA = REPLACE(DATA, @DB_BASEURL_OLD, "${DB_BASEURL}") where DATA LIKE CONCAT('%', @DB_BASEURL_OLD, '%');
UPDATE ${DB_DB}.BANDANA SET BANDANAVALUE = REPLACE(BANDANAVALUE, @DB_BASEURL_OLD, "${DB_BASEURL}") where BANDANAVALUE LIKE CONCAT('%', @DB_BASEURL_OLD, '%');
UPDATE ${DB_DB}.BODYCONTENT SET BODY = REPLACE(BODY, @DB_BASEURL_OLD, "${DB_BASEURL}") where BODY LIKE CONCAT('%', @DB_BASEURL_OLD, '%');
UPDATE ${DB_DB}.EXTRNLNKS SET LOWERURL = REPLACE(LOWERURL, @DB_BASEURL_OLD, "${DB_BASEURL}") where LOWERURL LIKE CONCAT('%', @DB_BASEURL_OLD, '%');
UPDATE ${DB_DB}.EXTRNLNKS SET URL = REPLACE(URL, @DB_BASEURL_OLD, "${DB_BASEURL}") where URL LIKE CONCAT('%', @DB_BASEURL_OLD, '%');
UPDATE ${DB_DB}.OS_PROPERTYENTRY SET string_val = REPLACE(string_val, @DB_BASEURL_OLD, "${DB_BASEURL}") where string_val LIKE CONCAT('%', @DB_BASEURL_OLD, '%');
EOF
# AO_9412A1_AOREGISTRATION --> possibly not existing in all installations
# AUDITRECORD              --> contains BaseURLs, but only for tracking of manual changes using the UI
# AUDIT_CHANGED_VALUE      --> contains BaseURLs, but only for tracking of manual changes using the UI
    fi
    if [ ${DB_DISABLEMAIL} = "true" ]; then
        cat << EOF >> ${DB_COMMANDS}
DELETE FROM ${DB_DB}.BANDANA WHERE BANDANAKEY="atlassian.confluence.smtp.mail.accounts";
EOF
    fi
    if [ ! -z "${DB_CUSTOMHTML}" ]; then
        cat << EOF >> ${DB_COMMANDS}
UPDATE BANDANA SET BANDANAVALUE = CONCAT(SUBSTR(BANDANAVALUE, 1, LOCATE('<afterBodyStart>', BANDANAVALUE)+15),'${DB_CUSTOMHTML}',SUBSTR(BANDANAVALUE, LOCATE('</afterBodyStart>', BANDANAVALUE))) WHERE BANDANAKEY="atlassian.confluence.settings";
EOF
    fi
# If there are changes that have to be applied at all
    if [ -f ${DB_COMMANDS} ]; then
        if [ -z "${DB}" ]; then
            DB=$(xmlstarlet sel -T -t -v "/confluence-configuration/properties/property[@name='hibernate.connection.url']" -n ${CONF_HOME}/confluence.cfg.xml | cut -d"/" -f3)
        fi
# Get username & password from ${CONF_HOME}/confluence.cfg.xml if no password is specified
        if [ -z "${DB_PASSWORD}" ]; then
            DB_PASSWORD=$(xmlstarlet sel -T -t -v "/confluence-configuration/properties/property[@name='hibernate.connection.password']" -n ${CONF_HOME}/confluence.cfg.xml)
            if [ -z "${DB_USER}" ]; then
                DB_USER=$(xmlstarlet sel -T -t -v "/confluence-configuration/properties/property[@name='hibernate.connection.username']" -n ${CONF_HOME}/confluence.cfg.xml)
            fi
        else
# Username is set to root, if a password but no username is specified
            if [ -z "${DB_USER}" ]; then
                DB_USER=root
            fi
        fi

        mysql -h${DB} -D${DB_DB} -u${DB_USER} -p${DB_PASSWORD} < ${DB_COMMANDS} && DB_ADJUSTMENTS_DONE=true
    fi
fi
if [ ${DB_ADJUSTMENTS_DONE} = "true" ]; then
    echo "The following SQL commands have been issued:"
    cat ${DB_COMMANDS}
else
    echo "No SQL commands have been issued"
fi

# Continue as unprivileged user
if [ -z ${DO_NOT_START} ]; then
    touch ${CONF_HOME}/DONTSYNC
    su -p -s /bin/bash -c "${CONF_INSTALL}/bin/configure-and-start.sh" ${RUN_USER}
fi
